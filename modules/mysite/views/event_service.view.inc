<?php

/**
 * @file
 * Defines the Events Service view.
 */

$view = new view();
$view->name = 'event_services';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Event Services';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Events';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
  'delete_node' => 'edit_node',
  'edit_node' => 'edit_node',
  'field_event_date' => 'field_event_date',
  'field_full_description' => 'field_full_description',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'delete_node' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'edit_node' => array(
    'align' => '',
    'separator' => ' ',
    'empty_column' => 0,
  ),
  'field_event_date' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_full_description' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_wrapper_type'] = '0';
/* Field: Content: Event Date */
$handler->display->display_options['fields']['field_event_date']['id'] = 'field_event_date';
$handler->display->display_options['fields']['field_event_date']['table'] = 'field_data_field_event_date';
$handler->display->display_options['fields']['field_event_date']['field'] = 'field_event_date';
$handler->display->display_options['fields']['field_event_date']['label'] = 'Date';
$handler->display->display_options['fields']['field_event_date']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_event_date']['settings'] = array(
  'format_type' => 'date_only',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'show',
);
$handler->display->display_options['fields']['field_event_date']['delta_offset'] = '0';
/* Field: Content: Full Description */
$handler->display->display_options['fields']['field_full_description']['id'] = 'field_full_description';
$handler->display->display_options['fields']['field_full_description']['table'] = 'field_data_field_full_description';
$handler->display->display_options['fields']['field_full_description']['field'] = 'field_full_description';
$handler->display->display_options['fields']['field_full_description']['label'] = 'Notes';
$handler->display->display_options['fields']['field_full_description']['element_wrapper_type'] = '0';
/* Field: Content: Edit link */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = 'Actions';
$handler->display->display_options['fields']['edit_node']['element_wrapper_type'] = 'span';
$handler->display->display_options['fields']['edit_node']['element_wrapper_class'] = 'edit-link';
/* Field: Content: Delete link */
$handler->display->display_options['fields']['delete_node']['id'] = 'delete_node';
$handler->display->display_options['fields']['delete_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['delete_node']['field'] = 'delete_node';
$handler->display->display_options['fields']['delete_node']['label'] = 'Delete';
$handler->display->display_options['fields']['delete_node']['element_wrapper_type'] = 'span';
$handler->display->display_options['fields']['delete_node']['element_wrapper_class'] = 'edit-link';
/* Sort criterion: Content: Event Date -  start date (field_event_date) */
$handler->display->display_options['sorts']['field_event_date_value']['id'] = 'field_event_date_value';
$handler->display->display_options['sorts']['field_event_date_value']['table'] = 'field_data_field_event_date';
$handler->display->display_options['sorts']['field_event_date_value']['field'] = 'field_event_date_value';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'event' => 'event',
);
/* Filter criterion: Content: Event Date -  start date (field_event_date) */
$handler->display->display_options['filters']['field_event_date_value']['id'] = 'field_event_date_value';
$handler->display->display_options['filters']['field_event_date_value']['table'] = 'field_data_field_event_date';
$handler->display->display_options['filters']['field_event_date_value']['field'] = 'field_event_date_value';
$handler->display->display_options['filters']['field_event_date_value']['operator'] = '>=';
$handler->display->display_options['filters']['field_event_date_value']['default_date'] = 'now - 30 days';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['access'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'administer nodes';
$handler->display->display_options['path'] = 'events';

/* Display: Event List Service */
$handler = $view->new_display('services', 'Event List Service', 'services_event_list');
$handler->display->display_options['display_description'] = 'Display a list of events';
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['label'] = 'nid';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: Event Date */
$handler->display->display_options['fields']['field_event_date']['id'] = 'field_event_date';
$handler->display->display_options['fields']['field_event_date']['table'] = 'field_data_field_event_date';
$handler->display->display_options['fields']['field_event_date']['field'] = 'field_event_date';
$handler->display->display_options['fields']['field_event_date']['label'] = 'event_date';
$handler->display->display_options['fields']['field_event_date']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_event_date']['type'] = 'services';
$handler->display->display_options['fields']['field_event_date']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 0,
);
$handler->display->display_options['fields']['field_event_date']['delta_offset'] = '0';
/* Field: Content: Academic Term */
$handler->display->display_options['fields']['field_event_term']['id'] = 'field_event_term';
$handler->display->display_options['fields']['field_event_term']['table'] = 'field_data_field_event_term';
$handler->display->display_options['fields']['field_event_term']['field'] = 'field_event_term';
$handler->display->display_options['fields']['field_event_term']['label'] = 'event_term';
$handler->display->display_options['fields']['field_event_term']['type'] = 'services';
$handler->display->display_options['fields']['field_event_term']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 1,
);
/* Field: Content: Display on Calendars */
$handler->display->display_options['fields']['field_display_on_calendars']['id'] = 'field_display_on_calendars';
$handler->display->display_options['fields']['field_display_on_calendars']['table'] = 'field_data_field_display_on_calendars';
$handler->display->display_options['fields']['field_display_on_calendars']['field'] = 'field_display_on_calendars';
$handler->display->display_options['fields']['field_display_on_calendars']['label'] = 'calendars';
$handler->display->display_options['fields']['field_display_on_calendars']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_display_on_calendars']['type'] = 'services';
$handler->display->display_options['fields']['field_display_on_calendars']['settings'] = array(
  'skip_safe' => 1,
  'skip_empty_values' => 1,
);
$handler->display->display_options['fields']['field_display_on_calendars']['delta_offset'] = '0';
/* Field: Content: Location */
$handler->display->display_options['fields']['field_location']['id'] = 'field_location';
$handler->display->display_options['fields']['field_location']['table'] = 'field_data_field_location';
$handler->display->display_options['fields']['field_location']['field'] = 'field_location';
$handler->display->display_options['fields']['field_location']['label'] = 'location';
$handler->display->display_options['fields']['field_location']['type'] = 'services';
$handler->display->display_options['fields']['field_location']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 1,
);
/* Field: Content: Short Description */
$handler->display->display_options['fields']['field_short_description']['id'] = 'field_short_description';
$handler->display->display_options['fields']['field_short_description']['table'] = 'field_data_field_short_description';
$handler->display->display_options['fields']['field_short_description']['field'] = 'field_short_description';
$handler->display->display_options['fields']['field_short_description']['label'] = 'short_description';
$handler->display->display_options['fields']['field_short_description']['type'] = 'services';
$handler->display->display_options['fields']['field_short_description']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 1,
);
/* Field: Content: Full Description */
$handler->display->display_options['fields']['field_full_description']['id'] = 'field_full_description';
$handler->display->display_options['fields']['field_full_description']['table'] = 'field_data_field_full_description';
$handler->display->display_options['fields']['field_full_description']['field'] = 'field_full_description';
$handler->display->display_options['fields']['field_full_description']['label'] = 'full_description';
$handler->display->display_options['fields']['field_full_description']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_full_description']['type'] = 'services';
$handler->display->display_options['fields']['field_full_description']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 1,
);
/* Field: Content: Link */
$handler->display->display_options['fields']['field_link']['id'] = 'field_link';
$handler->display->display_options['fields']['field_link']['table'] = 'field_data_field_link';
$handler->display->display_options['fields']['field_link']['field'] = 'field_link';
$handler->display->display_options['fields']['field_link']['label'] = 'link';
$handler->display->display_options['fields']['field_link']['click_sort_column'] = 'url';
$handler->display->display_options['fields']['field_link']['type'] = 'services';
$handler->display->display_options['fields']['field_link']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 1,
);
/* Field: Content: Related News or Announcement */
$handler->display->display_options['fields']['field_related_news']['id'] = 'field_related_news';
$handler->display->display_options['fields']['field_related_news']['table'] = 'field_data_field_related_news';
$handler->display->display_options['fields']['field_related_news']['field'] = 'field_related_news';
$handler->display->display_options['fields']['field_related_news']['label'] = 'related_news';
$handler->display->display_options['fields']['field_related_news']['type'] = 'services';
$handler->display->display_options['fields']['field_related_news']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 1,
);
/* Field: Content: Press Release */
$handler->display->display_options['fields']['field_press_release']['id'] = 'field_press_release';
$handler->display->display_options['fields']['field_press_release']['table'] = 'field_data_field_press_release';
$handler->display->display_options['fields']['field_press_release']['field'] = 'field_press_release';
$handler->display->display_options['fields']['field_press_release']['label'] = 'press_release';
$handler->display->display_options['fields']['field_press_release']['click_sort_column'] = 'url';
$handler->display->display_options['fields']['field_press_release']['type'] = 'services';
$handler->display->display_options['fields']['field_press_release']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 1,
);
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'event' => 'event',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Event Date -  start date (field_event_date) */
$handler->display->display_options['filters']['field_event_date_value']['id'] = 'field_event_date_value';
$handler->display->display_options['filters']['field_event_date_value']['table'] = 'field_data_field_event_date';
$handler->display->display_options['filters']['field_event_date_value']['field'] = 'field_event_date_value';
$handler->display->display_options['filters']['field_event_date_value']['operator'] = '>=';
$handler->display->display_options['filters']['field_event_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_event_date_value']['expose']['operator_id'] = 'field_event_date_value_op';
$handler->display->display_options['filters']['field_event_date_value']['expose']['label'] = 'event_date';
$handler->display->display_options['filters']['field_event_date_value']['expose']['operator'] = 'field_event_date_value_op';
$handler->display->display_options['filters']['field_event_date_value']['expose']['identifier'] = 'event_date';
$handler->display->display_options['filters']['field_event_date_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  6 => 0,
  4 => 0,
  5 => 0,
);
$handler->display->display_options['filters']['field_event_date_value']['default_date'] = 'now - 1 year';
/* Filter criterion: Content: Academic Term (field_event_term) */
$handler->display->display_options['filters']['field_event_term_value']['id'] = 'field_event_term_value';
$handler->display->display_options['filters']['field_event_term_value']['table'] = 'field_data_field_event_term';
$handler->display->display_options['filters']['field_event_term_value']['field'] = 'field_event_term_value';
$handler->display->display_options['filters']['field_event_term_value']['group'] = 1;
$handler->display->display_options['filters']['field_event_term_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_event_term_value']['expose']['operator_id'] = 'field_event_term_value_op';
$handler->display->display_options['filters']['field_event_term_value']['expose']['label'] = 'Academic Term';
$handler->display->display_options['filters']['field_event_term_value']['expose']['operator'] = 'field_event_term_value_op';
$handler->display->display_options['filters']['field_event_term_value']['expose']['identifier'] = 'term';
$handler->display->display_options['filters']['field_event_term_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
);
/* Filter criterion: Content: Display on Calendars (field_display_on_calendars) */
$handler->display->display_options['filters']['field_display_on_calendars_value']['id'] = 'field_display_on_calendars_value';
$handler->display->display_options['filters']['field_display_on_calendars_value']['table'] = 'field_data_field_display_on_calendars';
$handler->display->display_options['filters']['field_display_on_calendars_value']['field'] = 'field_display_on_calendars_value';
$handler->display->display_options['filters']['field_display_on_calendars_value']['value'] = array(
  '1a86eddc18a75fdb45bc5e3609851ee4' => '1a86eddc18a75fdb45bc5e3609851ee4',
);
$handler->display->display_options['filters']['field_display_on_calendars_value']['group'] = 1;
$handler->display->display_options['filters']['field_display_on_calendars_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_display_on_calendars_value']['expose']['operator_id'] = 'field_display_on_calendars_value_op';
$handler->display->display_options['filters']['field_display_on_calendars_value']['expose']['label'] = 'calendar';
$handler->display->display_options['filters']['field_display_on_calendars_value']['expose']['operator'] = 'field_display_on_calendars_value_op';
$handler->display->display_options['filters']['field_display_on_calendars_value']['expose']['identifier'] = 'calendar';
$handler->display->display_options['filters']['field_display_on_calendars_value']['expose']['required'] = TRUE;
$handler->display->display_options['filters']['field_display_on_calendars_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
);
$handler->display->display_options['path'] = 'events';

/* Display: Event Item Service */
$handler = $view->new_display('services', 'Event Item Service', 'services_event_item');
$handler->display->display_options['display_description'] = 'Display a single event item by ID';
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '1';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['label'] = 'nid';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: Event Date */
$handler->display->display_options['fields']['field_event_date']['id'] = 'field_event_date';
$handler->display->display_options['fields']['field_event_date']['table'] = 'field_data_field_event_date';
$handler->display->display_options['fields']['field_event_date']['field'] = 'field_event_date';
$handler->display->display_options['fields']['field_event_date']['label'] = 'event_date';
$handler->display->display_options['fields']['field_event_date']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_event_date']['type'] = 'services';
$handler->display->display_options['fields']['field_event_date']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 0,
);
$handler->display->display_options['fields']['field_event_date']['delta_offset'] = '0';
/* Field: Content: Academic Term */
$handler->display->display_options['fields']['field_event_term']['id'] = 'field_event_term';
$handler->display->display_options['fields']['field_event_term']['table'] = 'field_data_field_event_term';
$handler->display->display_options['fields']['field_event_term']['field'] = 'field_event_term';
$handler->display->display_options['fields']['field_event_term']['label'] = 'event_term';
$handler->display->display_options['fields']['field_event_term']['type'] = 'services';
$handler->display->display_options['fields']['field_event_term']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 1,
);
/* Field: Content: Display on Calendars */
$handler->display->display_options['fields']['field_display_on_calendars']['id'] = 'field_display_on_calendars';
$handler->display->display_options['fields']['field_display_on_calendars']['table'] = 'field_data_field_display_on_calendars';
$handler->display->display_options['fields']['field_display_on_calendars']['field'] = 'field_display_on_calendars';
$handler->display->display_options['fields']['field_display_on_calendars']['label'] = 'calendars';
$handler->display->display_options['fields']['field_display_on_calendars']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_display_on_calendars']['type'] = 'services';
$handler->display->display_options['fields']['field_display_on_calendars']['settings'] = array(
  'skip_safe' => 1,
  'skip_empty_values' => 1,
);
$handler->display->display_options['fields']['field_display_on_calendars']['delta_offset'] = '0';
/* Field: Content: Location */
$handler->display->display_options['fields']['field_location']['id'] = 'field_location';
$handler->display->display_options['fields']['field_location']['table'] = 'field_data_field_location';
$handler->display->display_options['fields']['field_location']['field'] = 'field_location';
$handler->display->display_options['fields']['field_location']['label'] = 'location';
$handler->display->display_options['fields']['field_location']['type'] = 'services';
$handler->display->display_options['fields']['field_location']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 1,
);
/* Field: Content: Short Description */
$handler->display->display_options['fields']['field_short_description']['id'] = 'field_short_description';
$handler->display->display_options['fields']['field_short_description']['table'] = 'field_data_field_short_description';
$handler->display->display_options['fields']['field_short_description']['field'] = 'field_short_description';
$handler->display->display_options['fields']['field_short_description']['label'] = 'short_description';
$handler->display->display_options['fields']['field_short_description']['type'] = 'services';
$handler->display->display_options['fields']['field_short_description']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 1,
);
/* Field: Content: Full Description */
$handler->display->display_options['fields']['field_full_description']['id'] = 'field_full_description';
$handler->display->display_options['fields']['field_full_description']['table'] = 'field_data_field_full_description';
$handler->display->display_options['fields']['field_full_description']['field'] = 'field_full_description';
$handler->display->display_options['fields']['field_full_description']['label'] = 'full_description';
$handler->display->display_options['fields']['field_full_description']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_full_description']['type'] = 'services';
$handler->display->display_options['fields']['field_full_description']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 1,
);
/* Field: Content: Link */
$handler->display->display_options['fields']['field_link']['id'] = 'field_link';
$handler->display->display_options['fields']['field_link']['table'] = 'field_data_field_link';
$handler->display->display_options['fields']['field_link']['field'] = 'field_link';
$handler->display->display_options['fields']['field_link']['label'] = 'link';
$handler->display->display_options['fields']['field_link']['click_sort_column'] = 'url';
$handler->display->display_options['fields']['field_link']['type'] = 'services';
$handler->display->display_options['fields']['field_link']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 1,
);
/* Field: Content: Related News or Announcement */
$handler->display->display_options['fields']['field_related_news']['id'] = 'field_related_news';
$handler->display->display_options['fields']['field_related_news']['table'] = 'field_data_field_related_news';
$handler->display->display_options['fields']['field_related_news']['field'] = 'field_related_news';
$handler->display->display_options['fields']['field_related_news']['label'] = 'related_news';
$handler->display->display_options['fields']['field_related_news']['type'] = 'services';
$handler->display->display_options['fields']['field_related_news']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 1,
);
/* Field: Content: Press Release */
$handler->display->display_options['fields']['field_press_release']['id'] = 'field_press_release';
$handler->display->display_options['fields']['field_press_release']['table'] = 'field_data_field_press_release';
$handler->display->display_options['fields']['field_press_release']['field'] = 'field_press_release';
$handler->display->display_options['fields']['field_press_release']['label'] = 'press_release';
$handler->display->display_options['fields']['field_press_release']['click_sort_column'] = 'url';
$handler->display->display_options['fields']['field_press_release']['type'] = 'services';
$handler->display->display_options['fields']['field_press_release']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 1,
);
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'event' => 'event',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Nid */
$handler->display->display_options['filters']['nid']['id'] = 'nid';
$handler->display->display_options['filters']['nid']['table'] = 'node';
$handler->display->display_options['filters']['nid']['field'] = 'nid';
$handler->display->display_options['filters']['nid']['group'] = 1;
$handler->display->display_options['filters']['nid']['exposed'] = TRUE;
$handler->display->display_options['filters']['nid']['expose']['operator_id'] = 'nid_op';
$handler->display->display_options['filters']['nid']['expose']['label'] = 'Nid';
$handler->display->display_options['filters']['nid']['expose']['operator'] = 'nid_op';
$handler->display->display_options['filters']['nid']['expose']['identifier'] = 'nid';
$handler->display->display_options['filters']['nid']['expose']['required'] = TRUE;
$handler->display->display_options['filters']['nid']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
);
$handler->display->display_options['path'] = 'event';
