<?php

/**
 * @file
 * Defines the News Entities Service (version2) view.
 */

$view = new view();
$view->name = 'news_entities_service_v2';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'News Entities Service v2';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '8';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['label'] = 'nid';
$handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
/* Field: Content: Legacy ID */
$handler->display->display_options['fields']['field_legacy_id']['id'] = 'field_legacy_id';
$handler->display->display_options['fields']['field_legacy_id']['table'] = 'field_data_field_legacy_id';
$handler->display->display_options['fields']['field_legacy_id']['field'] = 'field_legacy_id';
$handler->display->display_options['fields']['field_legacy_id']['label'] = 'legacy_id';
$handler->display->display_options['fields']['field_legacy_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_legacy_id']['type'] = 'number_unformatted';
$handler->display->display_options['fields']['field_legacy_id']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 0,
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: Photo */
$handler->display->display_options['fields']['field_photo']['id'] = 'field_photo';
$handler->display->display_options['fields']['field_photo']['table'] = 'field_data_field_photo';
$handler->display->display_options['fields']['field_photo']['field'] = 'field_photo';
$handler->display->display_options['fields']['field_photo']['label'] = 'photo_entity';
$handler->display->display_options['fields']['field_photo']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_photo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_photo']['type'] = 'services';
$handler->display->display_options['fields']['field_photo']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 0,
);
/* Field: photo_teaser */
$handler->display->display_options['fields']['field_photo_4']['id'] = 'field_photo_4';
$handler->display->display_options['fields']['field_photo_4']['table'] = 'field_data_field_photo';
$handler->display->display_options['fields']['field_photo_4']['field'] = 'field_photo';
$handler->display->display_options['fields']['field_photo_4']['ui_name'] = 'photo_teaser';
$handler->display->display_options['fields']['field_photo_4']['label'] = 'photo_teaser';
$handler->display->display_options['fields']['field_photo_4']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_photo_4']['settings'] = array(
  'image_style' => 'teaser',
  'image_link' => '',
);
/* Field: photo_thumbnail */
$handler->display->display_options['fields']['field_photo_1']['id'] = 'field_photo_1';
$handler->display->display_options['fields']['field_photo_1']['table'] = 'field_data_field_photo';
$handler->display->display_options['fields']['field_photo_1']['field'] = 'field_photo';
$handler->display->display_options['fields']['field_photo_1']['ui_name'] = 'photo_thumbnail';
$handler->display->display_options['fields']['field_photo_1']['label'] = 'photo_thumbnail';
$handler->display->display_options['fields']['field_photo_1']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_photo_1']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_photo_1']['settings'] = array(
  'image_style' => 'thumbnail',
  'image_link' => '',
);
/* Field: photo_medium */
$handler->display->display_options['fields']['field_photo_2']['id'] = 'field_photo_2';
$handler->display->display_options['fields']['field_photo_2']['table'] = 'field_data_field_photo';
$handler->display->display_options['fields']['field_photo_2']['field'] = 'field_photo';
$handler->display->display_options['fields']['field_photo_2']['ui_name'] = 'photo_medium';
$handler->display->display_options['fields']['field_photo_2']['label'] = 'photo_medium';
$handler->display->display_options['fields']['field_photo_2']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_photo_2']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_photo_2']['settings'] = array(
  'image_style' => 'medium',
  'image_link' => '',
);
/* Field: photo_large */
$handler->display->display_options['fields']['field_photo_3']['id'] = 'field_photo_3';
$handler->display->display_options['fields']['field_photo_3']['table'] = 'field_data_field_photo';
$handler->display->display_options['fields']['field_photo_3']['field'] = 'field_photo';
$handler->display->display_options['fields']['field_photo_3']['ui_name'] = 'photo_large';
$handler->display->display_options['fields']['field_photo_3']['label'] = 'photo_large';
$handler->display->display_options['fields']['field_photo_3']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_photo_3']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_photo_3']['settings'] = array(
  'image_style' => 'large',
  'image_link' => '',
);
/* Field: Content: Release Date */
$handler->display->display_options['fields']['field_release_date']['id'] = 'field_release_date';
$handler->display->display_options['fields']['field_release_date']['table'] = 'field_data_field_release_date';
$handler->display->display_options['fields']['field_release_date']['field'] = 'field_release_date';
$handler->display->display_options['fields']['field_release_date']['label'] = 'release_date';
$handler->display->display_options['fields']['field_release_date']['type'] = 'services';
$handler->display->display_options['fields']['field_release_date']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 0,
);
/* Field: Content: Cut line */
$handler->display->display_options['fields']['field_cut_line']['id'] = 'field_cut_line';
$handler->display->display_options['fields']['field_cut_line']['table'] = 'field_data_field_cut_line';
$handler->display->display_options['fields']['field_cut_line']['field'] = 'field_cut_line';
$handler->display->display_options['fields']['field_cut_line']['label'] = 'cut_line';
$handler->display->display_options['fields']['field_cut_line']['type'] = 'services';
$handler->display->display_options['fields']['field_cut_line']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 0,
);
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['label'] = 'body';
$handler->display->display_options['fields']['body']['type'] = 'services';
$handler->display->display_options['fields']['body']['settings'] = array(
  'skip_safe' => 1,
  'skip_empty_values' => 0,
);
/* Field: Content: Press Release */
$handler->display->display_options['fields']['field_press_release']['id'] = 'field_press_release';
$handler->display->display_options['fields']['field_press_release']['table'] = 'field_data_field_press_release';
$handler->display->display_options['fields']['field_press_release']['field'] = 'field_press_release';
$handler->display->display_options['fields']['field_press_release']['label'] = 'press_release';
$handler->display->display_options['fields']['field_press_release']['click_sort_column'] = 'url';
$handler->display->display_options['fields']['field_press_release']['type'] = 'services';
$handler->display->display_options['fields']['field_press_release']['settings'] = array(
  'skip_safe' => 1,
  'skip_empty_values' => 0,
);
/* Field: Content: Retire Date */
$handler->display->display_options['fields']['field_retire_date']['id'] = 'field_retire_date';
$handler->display->display_options['fields']['field_retire_date']['table'] = 'field_data_field_retire_date';
$handler->display->display_options['fields']['field_retire_date']['field'] = 'field_retire_date';
$handler->display->display_options['fields']['field_retire_date']['label'] = 'retire_date';
$handler->display->display_options['fields']['field_retire_date']['type'] = 'services';
$handler->display->display_options['fields']['field_retire_date']['settings'] = array(
  'skip_safe' => 1,
  'skip_empty_values' => 0,
);
/* Field: Promoted */
$handler->display->display_options['fields']['promote']['id'] = 'promote';
$handler->display->display_options['fields']['promote']['table'] = 'node';
$handler->display->display_options['fields']['promote']['field'] = 'promote';
$handler->display->display_options['fields']['promote']['ui_name'] = 'Promoted';
$handler->display->display_options['fields']['promote']['label'] = 'promoted';
$handler->display->display_options['fields']['promote']['type'] = 'boolean';
$handler->display->display_options['fields']['promote']['not'] = 0;
/* Field: Content: Weight */
$handler->display->display_options['fields']['field_weight']['id'] = 'field_weight';
$handler->display->display_options['fields']['field_weight']['table'] = 'field_data_field_weight';
$handler->display->display_options['fields']['field_weight']['field'] = 'field_weight';
$handler->display->display_options['fields']['field_weight']['label'] = 'weight';
$handler->display->display_options['fields']['field_weight']['type'] = 'services';
$handler->display->display_options['fields']['field_weight']['settings'] = array(
  'skip_safe' => 1,
  'skip_empty_values' => 0,
);
/* Field: Content: Websites */
$handler->display->display_options['fields']['field_websites']['id'] = 'field_websites';
$handler->display->display_options['fields']['field_websites']['table'] = 'field_data_field_websites';
$handler->display->display_options['fields']['field_websites']['field'] = 'field_websites';
$handler->display->display_options['fields']['field_websites']['label'] = 'newsfeeds';
$handler->display->display_options['fields']['field_websites']['type'] = 'services';
$handler->display->display_options['fields']['field_websites']['settings'] = array(
  'skip_safe' => 1,
  'skip_empty_values' => 1,
);
$handler->display->display_options['fields']['field_websites']['delta_offset'] = '0';
/* Sort criterion: Content: Release Date (field_release_date) */
$handler->display->display_options['sorts']['field_release_date_value']['id'] = 'field_release_date_value';
$handler->display->display_options['sorts']['field_release_date_value']['table'] = 'field_data_field_release_date';
$handler->display->display_options['sorts']['field_release_date_value']['field'] = 'field_release_date_value';
$handler->display->display_options['sorts']['field_release_date_value']['order'] = 'DESC';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news' => 'news',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Release Date (field_release_date) */
$handler->display->display_options['filters']['field_release_date_value']['id'] = 'field_release_date_value';
$handler->display->display_options['filters']['field_release_date_value']['table'] = 'field_data_field_release_date';
$handler->display->display_options['filters']['field_release_date_value']['field'] = 'field_release_date_value';
$handler->display->display_options['filters']['field_release_date_value']['operator'] = '<=';
$handler->display->display_options['filters']['field_release_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_release_date_value']['default_date'] = 'now';
/* Filter criterion: Content: Websites (field_websites) */
$handler->display->display_options['filters']['field_websites_value']['id'] = 'field_websites_value';
$handler->display->display_options['filters']['field_websites_value']['table'] = 'field_data_field_websites';
$handler->display->display_options['filters']['field_websites_value']['field'] = 'field_websites_value';
$handler->display->display_options['filters']['field_websites_value']['value'] = array(
  '4c176701c10a927b5866b6a1e8376ead' => '4c176701c10a927b5866b6a1e8376ead',
);
$handler->display->display_options['filters']['field_websites_value']['group'] = 1;
$handler->display->display_options['filters']['field_websites_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_websites_value']['expose']['operator_id'] = 'field_websites_value_op';
$handler->display->display_options['filters']['field_websites_value']['expose']['label'] = 'Website';
$handler->display->display_options['filters']['field_websites_value']['expose']['operator'] = 'field_websites_value_op';
$handler->display->display_options['filters']['field_websites_value']['expose']['identifier'] = 'website';
$handler->display->display_options['filters']['field_websites_value']['expose']['required'] = TRUE;
$handler->display->display_options['filters']['field_websites_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
);

/* Display: Recent News Service */
$handler = $view->new_display('services', 'Recent News Service', 'services_recent');
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '50';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['path'] = 'news-recent';

/* Display: News Archive Service */
$handler = $view->new_display('services', 'News Archive Service', 'services_archive');
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['label'] = 'nid';
$handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
/* Field: Content: Legacy ID */
$handler->display->display_options['fields']['field_legacy_id']['id'] = 'field_legacy_id';
$handler->display->display_options['fields']['field_legacy_id']['table'] = 'field_data_field_legacy_id';
$handler->display->display_options['fields']['field_legacy_id']['field'] = 'field_legacy_id';
$handler->display->display_options['fields']['field_legacy_id']['label'] = 'legacy_id';
$handler->display->display_options['fields']['field_legacy_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_legacy_id']['type'] = 'number_unformatted';
$handler->display->display_options['fields']['field_legacy_id']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 0,
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: Release Date */
$handler->display->display_options['fields']['field_release_date']['id'] = 'field_release_date';
$handler->display->display_options['fields']['field_release_date']['table'] = 'field_data_field_release_date';
$handler->display->display_options['fields']['field_release_date']['field'] = 'field_release_date';
$handler->display->display_options['fields']['field_release_date']['label'] = 'release_date';
$handler->display->display_options['fields']['field_release_date']['type'] = 'services';
$handler->display->display_options['fields']['field_release_date']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 0,
);
$handler->display->display_options['path'] = 'news-archive';

/* Display: News Item Service */
$handler = $view->new_display('services', 'News Item Service', 'services_news_item');
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '1';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['filter_groups']['groups'] = array(
  1 => 'AND',
  2 => 'OR',
);
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news' => 'news',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Release Date (field_release_date) */
$handler->display->display_options['filters']['field_release_date_value']['id'] = 'field_release_date_value';
$handler->display->display_options['filters']['field_release_date_value']['table'] = 'field_data_field_release_date';
$handler->display->display_options['filters']['field_release_date_value']['field'] = 'field_release_date_value';
$handler->display->display_options['filters']['field_release_date_value']['operator'] = '<=';
$handler->display->display_options['filters']['field_release_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_release_date_value']['default_date'] = 'now';
/* Filter criterion: Legacy ID */
$handler->display->display_options['filters']['field_legacy_id_value']['id'] = 'field_legacy_id_value';
$handler->display->display_options['filters']['field_legacy_id_value']['table'] = 'field_data_field_legacy_id';
$handler->display->display_options['filters']['field_legacy_id_value']['field'] = 'field_legacy_id_value';
$handler->display->display_options['filters']['field_legacy_id_value']['ui_name'] = 'Legacy ID';
$handler->display->display_options['filters']['field_legacy_id_value']['group'] = 2;
$handler->display->display_options['filters']['field_legacy_id_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_legacy_id_value']['expose']['operator_id'] = 'field_legacy_id_value_op';
$handler->display->display_options['filters']['field_legacy_id_value']['expose']['label'] = 'Legacy ID (field_legacy_id)';
$handler->display->display_options['filters']['field_legacy_id_value']['expose']['operator'] = 'field_legacy_id_value_op';
$handler->display->display_options['filters']['field_legacy_id_value']['expose']['identifier'] = 'legacy_id';
/* Filter criterion: Node ID */
$handler->display->display_options['filters']['nid']['id'] = 'nid';
$handler->display->display_options['filters']['nid']['table'] = 'node';
$handler->display->display_options['filters']['nid']['field'] = 'nid';
$handler->display->display_options['filters']['nid']['ui_name'] = 'Node ID';
$handler->display->display_options['filters']['nid']['group'] = 2;
$handler->display->display_options['filters']['nid']['exposed'] = TRUE;
$handler->display->display_options['filters']['nid']['expose']['operator_id'] = 'nid_op';
$handler->display->display_options['filters']['nid']['expose']['label'] = 'Nid';
$handler->display->display_options['filters']['nid']['expose']['operator'] = 'nid_op';
$handler->display->display_options['filters']['nid']['expose']['identifier'] = 'nid';
$handler->display->display_options['path'] = 'news-item';

/* Display: Weighted News Service */
$handler = $view->new_display('services', 'Weighted News Service', 'services_weighted');
$handler->display->display_options['display_description'] = 'Delivers a weighted list of promoted news items.';
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '50';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: Weight (field_weight) */
$handler->display->display_options['sorts']['field_weight_value']['id'] = 'field_weight_value';
$handler->display->display_options['sorts']['field_weight_value']['table'] = 'field_data_field_weight';
$handler->display->display_options['sorts']['field_weight_value']['field'] = 'field_weight_value';
/* Sort criterion: Content: Release Date (field_release_date) */
$handler->display->display_options['sorts']['field_release_date_value']['id'] = 'field_release_date_value';
$handler->display->display_options['sorts']['field_release_date_value']['table'] = 'field_data_field_release_date';
$handler->display->display_options['sorts']['field_release_date_value']['field'] = 'field_release_date_value';
$handler->display->display_options['sorts']['field_release_date_value']['order'] = 'DESC';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news' => 'news',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Promoted to front page */
$handler->display->display_options['filters']['promote']['id'] = 'promote';
$handler->display->display_options['filters']['promote']['table'] = 'node';
$handler->display->display_options['filters']['promote']['field'] = 'promote';
$handler->display->display_options['filters']['promote']['value'] = '1';
$handler->display->display_options['filters']['promote']['group'] = 1;
/* Filter criterion: Content: Release Date (field_release_date) */
$handler->display->display_options['filters']['field_release_date_value']['id'] = 'field_release_date_value';
$handler->display->display_options['filters']['field_release_date_value']['table'] = 'field_data_field_release_date';
$handler->display->display_options['filters']['field_release_date_value']['field'] = 'field_release_date_value';
$handler->display->display_options['filters']['field_release_date_value']['operator'] = '<=';
$handler->display->display_options['filters']['field_release_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_release_date_value']['default_date'] = 'now';
/* Filter criterion: Content: Retire Date (field_retire_date) */
$handler->display->display_options['filters']['field_retire_date_value']['id'] = 'field_retire_date_value';
$handler->display->display_options['filters']['field_retire_date_value']['table'] = 'field_data_field_retire_date';
$handler->display->display_options['filters']['field_retire_date_value']['field'] = 'field_retire_date_value';
$handler->display->display_options['filters']['field_retire_date_value']['operator'] = '>=';
$handler->display->display_options['filters']['field_retire_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_retire_date_value']['default_date'] = 'now';
/* Filter criterion: Content: Websites (field_websites) */
$handler->display->display_options['filters']['field_websites_value']['id'] = 'field_websites_value';
$handler->display->display_options['filters']['field_websites_value']['table'] = 'field_data_field_websites';
$handler->display->display_options['filters']['field_websites_value']['field'] = 'field_websites_value';
$handler->display->display_options['filters']['field_websites_value']['value'] = array(
  '4c176701c10a927b5866b6a1e8376ead' => '4c176701c10a927b5866b6a1e8376ead',
);
$handler->display->display_options['filters']['field_websites_value']['group'] = 1;
$handler->display->display_options['filters']['field_websites_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_websites_value']['expose']['operator_id'] = 'field_websites_value_op';
$handler->display->display_options['filters']['field_websites_value']['expose']['label'] = 'Website';
$handler->display->display_options['filters']['field_websites_value']['expose']['operator'] = 'field_websites_value_op';
$handler->display->display_options['filters']['field_websites_value']['expose']['identifier'] = 'website';
$handler->display->display_options['filters']['field_websites_value']['expose']['required'] = TRUE;
$handler->display->display_options['filters']['field_websites_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
);
$handler->display->display_options['path'] = 'news-weighted';
