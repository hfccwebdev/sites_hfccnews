<?php

/**
 * @file
 * Defines the Bulletin Service view.
 */

$view = new view();
$view->name = 'bulletin_service';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Bulletin Service';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['label'] = 'nid';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['label'] = 'body';
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'bulletin' => 'bulletin',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Promoted to front page */
$handler->display->display_options['filters']['promote']['id'] = 'promote';
$handler->display->display_options['filters']['promote']['table'] = 'node';
$handler->display->display_options['filters']['promote']['field'] = 'promote';
$handler->display->display_options['filters']['promote']['value'] = '1';
$handler->display->display_options['filters']['promote']['group'] = 1;
/* Filter criterion: Content: Release Date (field_release_date) */
$handler->display->display_options['filters']['field_release_date_value']['id'] = 'field_release_date_value';
$handler->display->display_options['filters']['field_release_date_value']['table'] = 'field_data_field_release_date';
$handler->display->display_options['filters']['field_release_date_value']['field'] = 'field_release_date_value';
$handler->display->display_options['filters']['field_release_date_value']['operator'] = '<=';
$handler->display->display_options['filters']['field_release_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_release_date_value']['default_date'] = 'now';
/* Filter criterion: Content: Retire Date (field_retire_date) */
$handler->display->display_options['filters']['field_retire_date_value']['id'] = 'field_retire_date_value';
$handler->display->display_options['filters']['field_retire_date_value']['table'] = 'field_data_field_retire_date';
$handler->display->display_options['filters']['field_retire_date_value']['field'] = 'field_retire_date_value';
$handler->display->display_options['filters']['field_retire_date_value']['operator'] = '>=';
$handler->display->display_options['filters']['field_retire_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_retire_date_value']['default_date'] = 'now';
/* Filter criterion: Content: Websites (field_websites) */
$handler->display->display_options['filters']['field_websites_value']['id'] = 'field_websites_value';
$handler->display->display_options['filters']['field_websites_value']['table'] = 'field_data_field_websites';
$handler->display->display_options['filters']['field_websites_value']['field'] = 'field_websites_value';
$handler->display->display_options['filters']['field_websites_value']['value'] = array(
  'www' => 'www',
);
$handler->display->display_options['filters']['field_websites_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_websites_value']['expose']['operator_id'] = 'field_websites_value_op';
$handler->display->display_options['filters']['field_websites_value']['expose']['label'] = 'Target Site';
$handler->display->display_options['filters']['field_websites_value']['expose']['operator'] = 'field_websites_value_op';
$handler->display->display_options['filters']['field_websites_value']['expose']['identifier'] = 'website';

/* Display: Portal Bulletin Service */
$handler = $view->new_display('services', 'Portal Bulletin Service', 'services_1');
$handler->display->display_options['display_description'] = 'Display ';
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['path'] = 'bulletins';
