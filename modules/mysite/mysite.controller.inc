<?php

/**
 * @file
 * Interfaces and Controllers for HFC Newsroom entities.
 */

/**
 * Calendar class.
 */
class HfccCalendarEntity extends Entity {
  protected function defaultLabel() {
    return check_plain($this->title);
  }
}

/**
 * HfccCalendarEntityController extends EntityAPIController.
 *
 * Our subclass of EntityAPIController lets us add a few
 * customizations to create, update, and delete methods.
 */
class HfccCalendarEntityController extends EntityAPIController {

  /**
   * Create and return a new entity.
   */
  public function create(array $values = array()) {
    $values += array(
      'api_key' => drupal_random_key(),
      'title' => NULL,
      'active' => 1,
    );
    return parent::create($values);
  }

}


/**
 * Client class.
 */
class HfccClientEntity extends Entity {
  protected function defaultLabel() {
    return check_plain($this->title);
  }
}

/**
 * HfccClientEntityController extends EntityAPIController.
 *
 * Our subclass of EntityAPIController lets us add a few
 * customizations to create, update, and delete methods.
 */
class HfccClientEntityController extends EntityAPIController {

  /**
   * Create and return a new entity.
   */
  public function create(array $values = array()) {
    $values += array(
      'api_key' => drupal_random_key(),
      'title' => NULL,
      'active' => 1,
    );
    return parent::create($values);
  }

}
