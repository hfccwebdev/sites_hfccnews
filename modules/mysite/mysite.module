<?php

/**
 * @file
 * Site-specific code.
 */

/**
 * Implements hook_menu().
 */
function mysite_menu() {
  return array(
    'admin/config/system/mysite' => array(
      'title' => 'Administer site-specific settings',
      'description' => 'Administer custom settings for <em>this</em> website.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('mysite_admin_form'),
      'access arguments' => array('administer site configuration'),
      'type' => MENU_NORMAL_ITEM,
      'file' => 'mysite.admin.inc',
    ),
    'admin/config/system/mysite/calendars' => array(
      'title' => 'Calendars',
      'description' => 'Manage client calendar keys.',
      'page callback' => 'mysite_calendars_admin',
      'access arguments' => array('administer site configuration'),
      'type' => MENU_LOCAL_TASK,
      'weight' => '2',
      'file' => 'mysite.admin.inc',
    ),
    'admin/config/system/mysite/calendars/add' => array(
      'title' => 'Add client calendar',
      'description' => 'Add a new client calendar.',
      'page callback' => 'mysite_calendar_add',
      'access callback' => 'entity_access',
      'access arguments' => array('update', 'hfcc_calendars', 2),
      'type' => MENU_LOCAL_ACTION,
      'file' => 'mysite.admin.inc',
    ),
    'admin/config/system/mysite/calendars/edit/%mysite_calendar' => array(
      'title' => 'Edit Calendar',
      'description' => 'Edit client calendar info.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('mysite_calendar_admin_form', 6),
      'access callback' => 'entity_access',
      'access arguments' => array('update', 'hfcc_calendars', 2),
      'type' => MENU_LOCAL_TASK,
      'file' => 'mysite.admin.inc',
    ),
    'admin/config/system/mysite/calendars/list' => array(
      'title' => 'List',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
    ),
    'admin/config/system/mysite/clients' => array(
      'title' => 'Clients',
      'description' => 'Manage client site keys.',
      'page callback' => 'mysite_clients_admin',
      'access arguments' => array('administer site configuration'),
      'type' => MENU_LOCAL_TASK,
      'weight' => '1',
      'file' => 'mysite.admin.inc',
    ),
    'admin/config/system/mysite/clients/add' => array(
      'title' => 'Add client site',
      'description' => 'Add a new client site.',
      'page callback' => 'mysite_client_add',
      'access callback' => 'entity_access',
      'access arguments' => array('update', 'hfcc_news_clients', 2),
      'type' => MENU_LOCAL_ACTION,
      'file' => 'mysite.admin.inc',
    ),
    'admin/config/system/mysite/clients/edit/%mysite_client' => array(
      'title' => 'Edit client site',
      'description' => 'Edit client site info.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('mysite_client_admin_form', 6),
      'access callback' => 'entity_access',
      'access arguments' => array('update', 'hfcc_news_clients', 2),
      'type' => MENU_LOCAL_TASK,
      'file' => 'mysite.admin.inc',
    ),
    'admin/config/system/mysite/clients/list' => array(
      'title' => 'List',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
    ),
    'admin/config/system/mysite/settings' => array(
      'title' => 'Settings',
      'weight' => '-10',
      'type' => MENU_DEFAULT_LOCAL_TASK,
    ),
    'events/%node' => array(
      // This is a duplicate of node/%node from node_menu().
      // It is here so links to events/nid work when used in
      // news stories, bulletins, or other events.
      'title callback' => 'node_page_title',
      'title arguments' => array(1),
      'page callback' => 'node_page_view',
      'page arguments' => array(1),
      'access callback' => 'node_access',
      'access arguments' => array('view', 1),
    ),
    'legacy/mainpic' => array(
      'title' => 'Legacy homepage random photo',
      'page callback' => 'mysite_legacy_mainpic',
      'access arguments' => array('access content'),
      'type' => MENU_CALLBACK,
      'file' => 'mysite.legacy.inc',
    ),
    'legacy/news/%' => array(
      'title' => 'Legacy news display',
      'page callback' => 'mysite_legacy_news_display',
      'page arguments' => array(2),
      'access arguments' => array('access content'),
      'type' => MENU_CALLBACK,
      'file' => 'mysite.legacy.inc',
    ),
    'legacy/titles/%' => array(
      'title' => 'Legacy title display',
      'page callback' => 'mysite_legacy_news_title',
      'page arguments' => array(2),
      'access arguments' => array('access content'),
      'type' => MENU_CALLBACK,
      'file' => 'mysite.legacy.inc',
    ),
    'newsroom/%' => array(
      'title' => 'Generic newsroom interface',
      'page callback' => 'mysite_newsroom_interface',
      'page arguments' => array(1),
      'access arguments' => array('access content'),
      'type' => MENU_CALLBACK,
      'file' => 'mysite.legacy.inc',
    ),
    'newsroom/%/%' => array(
      'title' => 'Generic newsroom interface',
      'page callback' => 'mysite_newsroom_interface',
      'page arguments' => array(1, 2),
      'access arguments' => array('access content'),
      'type' => MENU_CALLBACK,
      'file' => 'mysite.legacy.inc',
    ),
  );
}

/**
 * Implements hook_menu_alter().
 */
function mysite_menu_alter(&$items) {
  $items['node']['page callback'] = 'mysite_homepage';
}

/**
 * Page callback for custom homepage.
 */
function mysite_homepage() {
  drupal_set_title('');
  return '';
}

/**
 * Implements hook_views_api().
 */
function mysite_views_api() {
  return array(
    'api' => 3,
  );
}

/**
 * Implements hook_form_alter().
 */
function mysite_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'news_node_form':
      if (empty($form['nid']['#value'])) {
        $form['field_legacy_id'][LANGUAGE_NONE][0]['value']['#default_value'] = time();
      }
      $form['field_legacy_id'][LANGUAGE_NONE]['#disabled'] = TRUE;
      $form['field_front_weight'][LANGUAGE_NONE]['#disabled'] = TRUE;
      break;
    case 'user_login':
      $form['name']['#description'] = t('Enter your HFC username.');
      $form['pass']['#description'] = t('Enter your HFC password.');
      break;
    case 'user_login_block':
      $form['name']['#description'] = t('Enter your HFC username.');
      $form['pass']['#description'] = t('Enter your HFC password.');
      break;
  }
}

/**
 * Implements hook_field_widget_form_alter().
 *
 * @see http://drupal.stackexchange.com/questions/54906/what-hook-to-use-changing-image-field-description
 */
function mysite_field_widget_form_alter(&$element, &$form_state, $context) {
  if ($context['field']['type'] == 'image' && $context['instance']['widget']['type'] == 'image_image' && !empty($context['instance']['settings']['alt_field'])) {
    foreach (element_children($element) as $delta) {
      $element[$delta]['#process'][] = 'mysite_field_widget_process';
    }
  }
}

/**
 * Custom callback for image field process.
 */
function mysite_field_widget_process($element, &$form_state, $form) {
  // Verify the elements are accessible to the currently logged-in user before making any changes.
  // If so, force them to be required.
  if ($element['alt']['#access']) {
    $element['alt']['#required'] = TRUE;
  }
  if ($element['title']['#access']) {
    $element['title']['#required'] = TRUE;
  }

  return $element;
}

/**
 * Implements hook_entity_info().
 *
 * @see http://www.istos.it/blog/drupal-entities/drupal-entities-part-3-programming-hello-drupal-entity
 * @see http://drupal.org/node/1026420
 */
function mysite_entity_info() {
  return array(
    'hfcc_calendars' => array(
      'label' => t('HFC Calendars'),
      'entity class' => 'HfccCalendarEntity',
      'controller class' => 'HfccCalendarEntityController',
      'views controller class' => 'EntityDefaultViewsController',
      'base table' => 'hfcc_calendars',
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'access callback' => 'mysite_access',
      'fieldable' => FALSE,
      'entity keys' => array(
        'id' => 'api_key',
      ),
      'static cache' => TRUE,
      'bundles' => array(
        'hfcc_calendars' => array(
          'label' => 'HFC Calendars',
        ),
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('HFC Calendars'),
          'custom settings' => FALSE,
        ),
      ),
    ),
    'hfcc_news_clients' => array(
      'label' => t('HFC News Clients'),
      'entity class' => 'HfccClientEntity',
      'controller class' => 'HfccClientEntityController',
      'views controller class' => 'EntityDefaultViewsController',
      'base table' => 'hfcc_news_clients',
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'access callback' => 'mysite_access',
      'fieldable' => FALSE,
      'entity keys' => array(
        'id' => 'api_key',
      ),
      'static cache' => TRUE,
      'bundles' => array(
        'hfcc_news_clients' => array(
          'label' => 'HFC News Clients',
        ),
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('HFC News Clients'),
          'custom settings' => FALSE,
        ),
      ),
    ),
  );
}

/**
 * Access callback for mysite.
 *
 * Arguments for this function are defined by entity_access().
 * @see entity_access()
 */
function mysite_access($op, $entity = NULL, $account = NULL, $entity_type = NULL) {
  switch ($op) {
    case 'view':
      return user_access('access content');
    case 'create':
    case 'update':
    case 'delete':
      return user_access('administer site configuration');
  }
}

/**
 * Load a single calendar entity record.
 *
 * @param $id
 *    The id representing the record we want to load.
 */
function mysite_calendar_load($id) {
  $entity = entity_load('hfcc_calendars', array($id));
  return $entity ? reset($entity) : FALSE;
}

/**
 * Load a single client entity record.
 *
 * @param $id
 *    The id representing the record we want to load.
 */
function mysite_client_load($id) {
  $entity = entity_load('hfcc_news_clients', array($id));
  return $entity ? reset($entity) : FALSE;
}

/**
 * Returns a list of HFC calendars for use as field options.
 */
function mysite_hfcc_calendars_options() {
  $result = db_query("SELECT api_key, title FROM {hfcc_calendars} WHERE active=1 ORDER BY title ASC")->fetchAll();
  $options = array();
  foreach ($result as $value) {
    $options[$value->api_key] = $value->title;
  }
  return $options;
}

/**
 * Returns a list of HFC calendars for use as field options.
 */
function mysite_hfcc_news_clients_options() {
  $result = db_query("SELECT api_key, title FROM {hfcc_news_clients} WHERE active=1 ORDER BY title ASC")->fetchAll();
  $options = array();
  foreach ($result as $value) {
    $options[$value->api_key] = $value->title;
  }
  return $options;
}

/**
 * Implements hook_services_resources().
 */
function mysite_services_resources() {
  return array(
    'calendars' => array(
      'index' => array(
        'help' => 'Retrieve list of available calendars',
        'callback' => '_mysite_export_calendar_index',
        'access callback' => 'user_access',
        'access arguments' => array('access content'),
      ),
    ),
    'newsfeeds' => array(
      'index' => array(
        'help' => 'Retrieve list of available newsfeeds',
        'callback' => '_mysite_export_newsfeed_index',
        'access callback' => 'user_access',
        'access arguments' => array('access content'),
      ),
    ),
  );
}

/**
 * Returns a list of available calendars.
 */
function _mysite_export_calendar_index() {
  $result = db_query("SELECT * FROM {hfcc_calendars}")->fetchAll();
  return $result;
}

/**
 * Returns a list of available newsfeeds.
 */
function _mysite_export_newsfeed_index() {
  $result = db_query("SELECT * FROM {hfcc_news_clients}")->fetchAll();
  return $result;
}
