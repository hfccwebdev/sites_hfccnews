<?php

/**
 * @file
 * Contains functions for supporting the legacy website.
 *
 * @see mysite.module
 */

/**
 * Page callback for legacy main photo display.
 *
 * We want this to return a random pic every time, so temporarily
 * unset global caching.
 *
 * @see http://drupal.org/node/23797
 * @see http://drupal.org/project/cacheexclude
 *
 */
function mysite_legacy_mainpic() {
  $GLOBALS['conf']['cache'] = 0;
  echo views_embed_view('photo_gallery', 'block_random_photo');
}

/**
 * Page callback for legacy news article display.
 */
function mysite_legacy_news_display($legacyid) {
  switch ($legacyid) {
    case 'titles':
      echo views_embed_view('news', 'block_1');
      break;
    case 'recent':
      echo views_embed_view('news', 'block_legacy_ron');
      break;
    case 'archive':
      echo views_embed_view('news', 'page_1');
      break;
    case 'rss':
      mysite_legacy_news_feed();
      break;
    default:
      echo views_embed_view('news', 'block_legacy_article', $legacyid);
  }
}

/**
 * Page callback for legacy news title display.
 */
function mysite_legacy_news_title($legacyid) {
  $q = "SELECT node.title AS node_title, node.created AS node_created FROM {node} node ";
  $q .= "LEFT JOIN {field_data_field_legacy_id} field_data_field_legacy_id ON node.nid = field_data_field_legacy_id.entity_id AND (field_data_field_legacy_id.entity_type = 'node' AND field_data_field_legacy_id.deleted = '0') ";
  $q .= "WHERE (( (field_data_field_legacy_id.field_legacy_id_value = :id ) )AND(( (node.status = '1') AND (node.type IN  ('news')) ))) ";
  $q .= "ORDER BY node_created DESC LIMIT 1 OFFSET 0";

  $result = db_query($q, array(':id' => $legacyid))->fetchObject();
  if (!empty($result->node_title)) {
    echo $result->node_title;
  }
}

/**
 * Page callback for legacy news RSS feed.
 *
 * @see node_feed()
 */
function mysite_legacy_news_feed($apikey = NULL) {
  global $base_url, $language_content;

  if (empty($apikey)) {
    $apikey = variable_get('mysite_default_api_site', NULL);
  }
  $item_length = variable_get('feed_item_length', 'fulltext');
  $namespaces = array('xmlns:dc' => 'http://purl.org/dc/elements/1.1/');
  $teaser = ($item_length == 'teaser');
  $items = '';

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'news')
    ->propertyCondition('status', 1)
    ->fieldCondition('field_release_date', 'value', time(), '<')
    ->propertyOrderBy('created', 'DESC')
    ->addTag('node_access')
    ->range(0,20);

    if (!empty($apikey)) {
      $query->fieldCondition('field_websites', 'value', $apikey, '=');
    }

  $result = $query->execute();

  if (isset($result['node'])) {
    $nids = array_keys($result['node']);

    // Load all nodes to be rendered.
    $nodes = node_load_multiple($nids);
    foreach ($nodes as $node) {
      $item_text = '';

      $node->link = url('news/items/' . $node->field_legacy_id['und'][0]['value'], array('absolute' => TRUE));
      $node->rss_namespaces = array();
      $node->rss_elements = array(
        array('key' => 'pubDate', 'value' => gmdate('r', $node->field_release_date['und'][0]['value'])),
        array('key' => 'guid', 'value' => $base_url . '/news/items/' . $node->field_legacy_id['und'][0]['value'], 'attributes' => array('isPermaLink' => 'false'))
      );

      // The node gets built and modules add to or modify $node->rss_elements
      // and $node->rss_namespaces.
      $build = node_view($node, 'rss');
      unset($build['#theme']);

      if (!empty($node->rss_namespaces)) {
        $namespaces = array_merge($namespaces, $node->rss_namespaces);
      }

      if ($item_length != 'title') {
        // We render node contents and force links to be last.
        $build['links']['#weight'] = 1000;
        $item_text .= drupal_render($build);
      }

      $items .= format_rss_item($node->title, $node->link, $item_text, $node->rss_elements);
    }
  }

  $channel = array(
    'version'     => '2.0',
    'title'       => 'Henry Ford Community College News',
    'link'        => $base_url . '/news',
    'description' => 'The latest news from HFC.',
    'language'    => 'en-us',
  );
  $channel_extras = array();

  $output = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
  $output .= "<rss version=\"" . $channel["version"] . "\" xml:base=\"" . $base_url . "\" " . drupal_attributes($namespaces) . ">\n";
  $output .= format_rss_channel($channel['title'], $channel['link'], $channel['description'], $items, $channel['language'], $channel_extras);
  $output .= "</rss>\n";

  drupal_add_http_header('Content-Type', 'application/rss+xml; charset=utf-8');
  print $output;
}

/**
 * Expose a generic interface to newsroom content.
 */
function mysite_newsroom_interface($location_id, $target_site = NULL) {
  switch ($location_id) {
    case 'bulletins':
      echo views_embed_view('bulletins', 'block', $target_site);
      break;
  }
}
