<?php

/**
 * @file
 * Administration pages for custom settings on this website.
 *
 * @see mysite.module
 */

/**
 * Page callback for the settings form.
 */
function mysite_admin_form($form, &$form_state) {
  $form = array();
  $form[] = array(
    '#type' => 'item',
    '#markup' => t('Custom settings for this website.'),
  );
  $form['mysite_default_api_site'] = array(
    '#type' => 'select',
    '#title' => t('Default news client name'),
    '#options' => mysite_hfcc_news_clients_options(),
    '#default_value' => variable_get('mysite_default_api_site', NULL),
    '#description' => t('Select the default news client for this site. (See news administrator for details.)'),
  );
  $form['mysite_default_calendar_site'] = array(
    '#type' => 'select',
    '#title' => t('Default calendar name'),
    '#options' => mysite_hfcc_calendars_options(),
    '#default_value' => variable_get('mysite_default_calendar_site', NULL),
    '#description' => t('Select the default calendar for this site. (See news administrator for details.)'),
  );
  return system_settings_form($form);
}

/**
 * Administration page for client calendars.
 */
function mysite_calendars_admin() {
  $result = db_query("SELECT api_key, title, active FROM {hfcc_calendars} ORDER BY active DESC, title ASC")->fetchAll();
  $rows = array();
  foreach ($result as $value) {
    $rows[] = array(
      $value->title,
      $value->api_key,
      $value->active,
      l(t('edit'), 'admin/config/system/mysite/calendars/edit/' . $value->api_key),
    );
  }
  return array(
    '#theme' => 'table',
    '#header' => array(t('Title'), t('API Key'), t('Active'), t('Actions')),
    '#rows' => $rows,
  );
}

/**
 * Add new client calendar.
 */
function mysite_calendar_add() {
  $entity = entity_create('hfcc_calendars', array());
  return drupal_get_form('mysite_calendar_admin_form', $entity);
}

/**
 * Edit form for client calendar.
 */
function mysite_calendar_admin_form($form, &$form_state, $entity) {
  $form_title = $entity->label();
  drupal_set_title(!empty($form_title) ? $form_title : t('Add new calendar'));

  $form = array();

  $form['entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  $form['api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#description' => t('The key value for this client calendar.'),
    '#default_value' => $entity->api_key,
    '#required' => TRUE,
    '#disabled' => isset($entity->is_new) ? !$entity->is_new : TRUE,
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The title of this client calendar.'),
    '#default_value' => !empty($entity->title) ? $entity->title : NULL,
    '#required' => TRUE,
  );

  $form['active'] = array(
    '#type' => 'radios',
    '#title' => t('Active'),
    '#description' => t('Controls availability of this client calendar.'),
    '#options' => array(1 => t('Yes'), 0 => t('No')),
    '#default_value' => !empty($entity->active) ? $entity->active : 0,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Calendar edit form submit.
 */
function mysite_calendar_admin_form_submit($form, &$form_state) {
  $entity = $form_state['values']['entity'];
  if (isset($entity->is_new) && $entity->is_new) {
    $entity->api_key = $form_state['values']['api_key'];
  }
  $entity->title = $form_state['values']['title'];
  $entity->active = $form_state['values']['active'];
  entity_save('hfcc_calendars', $entity);
  $form_state['redirect'] = 'admin/config/system/mysite/calendars';
}

/**
 * Administration page for client sites.
 */
function mysite_clients_admin() {
  $result = db_query("SELECT api_key, title, active FROM {hfcc_news_clients} ORDER BY active DESC, title ASC")->fetchAll();
  $rows = array();
  foreach ($result as $value) {
    $rows[] = array(
      $value->title,
      $value->api_key,
      $value->active,
      l(t('edit'), 'admin/config/system/mysite/clients/edit/' . $value->api_key),
    );
  }
  return array(
    '#theme' => 'table',
    '#header' => array(t('Title'), t('API Key'), t('Active'), t('Actions')),
    '#rows' => $rows,
  );
}

/**
 * Add new client site.
 */
function mysite_client_add() {
  $entity = entity_create('hfcc_news_clients', array());
  return drupal_get_form('mysite_client_admin_form', $entity);
}

/**
 * Edit form for client site.
 */
function mysite_client_admin_form($form, &$form_state, $entity) {
  $form_title = $entity->label();
  drupal_set_title(!empty($form_title) ? $form_title : t('Add new site'));

  $form = array();

  $form['entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  $form['api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#description' => t('The key value for this client site.'),
    '#default_value' => $entity->api_key,
    '#required' => TRUE,
    '#disabled' => isset($entity->is_new) ? !$entity->is_new : TRUE,
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The title of this client site.'),
    '#default_value' => !empty($entity->title) ? $entity->title : NULL,
    '#required' => TRUE,
  );

  $form['active'] = array(
    '#type' => 'radios',
    '#title' => t('Active'),
    '#description' => t('Controls availability of this client site.'),
    '#options' => array(1 => t('Yes'), 0 => t('No')),
    '#default_value' => !empty($entity->active) ? $entity->active : 0,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Client edit form submit.
 */
function mysite_client_admin_form_submit($form, &$form_state) {
  $entity = $form_state['values']['entity'];
  if (isset($entity->is_new) && $entity->is_new) {
    $entity->api_key = $form_state['values']['api_key'];
  }
  $entity->title = $form_state['values']['title'];
  $entity->active = $form_state['values']['active'];
  entity_save('hfcc_news_clients', $entity);
  $form_state['redirect'] = 'admin/config/system/mysite/clients';
}
